<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductUnique extends Model
{
    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'used_at', 'returned_at'];

    protected $guarded = array();

    public static $rules = array();

    public function product()
    {
        return $this->belongsTo( 'App\Product', 'product_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function usages()
    {
        return $this->belongsToMany('Usage', 'product_unique_usage')
            ->withPivot('type', 'id')
            ->withTimestamps();
    }

    public function returnShipments()
    {
        return $this->belongsToMany('App\ReturnShipment', 'product_unique_return_shipment')
            ->withTimestamps();
    }

    public function scopeSearch($query, $search)
    {
        return $query->where(
            function($q) use ($search) {
                $q->where('products.productcode', 'LIKE', '%' . $search . '%')
                    ->orWhere('products.name', 'LIKE', '%' . $search . '%')
                    ->orWhere('product_uniques.lot', 'LIKE', '%' . $search . '%');
            }
        );
    }

    public function scopeExpiresBetween($query, $start, $end)
    {
        return $query->where('product_uniques.expires_at', '>=', $start)
            ->where('product_uniques.expires_at', '<', $end)
            ->excludeUsed();
    }

    public function scopeExpiresAfter($query, $date)
    {
        return $query->where('product_uniques.expires_at', '>=', $date)
            ->excludeUsed();
    }

    public function scopeExpired($query)
    {
        return $query->where('product_uniques.expires_at', '<', date('Y-m-d'))
            //->where('product_uniques.expires_at', '<>', '0000-00-00 00:00:00')
            ->whereNotNull('product_uniques.expires_at')
            ->excludeUsed();
    }

    public function scopeNeverExpires($query)
    {
        return $query->where(function ($query) {
            return $query->whereNull('product_uniques.expires_at')
                ->orWhere('product_uniques.expires_at', '0000-00-00');
        })->excludeUsed();
    }

    public function scopeFromOrganisation($query, $organisation_id)
    {
        return $query->whereHas('Product', function($query) use ($organisation_id) {
            return $query->where('products.organisation_id', $organisation_id);
        });
    }

    public function scopeUsedOnDate($query, $date = null)
    {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        return $query->where('product_uniques.used_at', '>=', $date)
            ->where('product_uniques.used_at', '<', date('Y-m-d', strtotime($date .'+1day')));
    }

    public function scopeUsedBetweenDates($query, $dateFrom, $dateTill = null)
    {
        if (is_null($dateTill)) {
            $dateTill = date('Y-m-d');
        }

        return $query->where('product_uniques.used_at', '>=', $dateFrom)
            ->where('product_uniques.used_at', '<', $dateTill);
    }

    public function scopeGrouped($query)
    {
        return $query->select(DB::raw('product_uniques.lot, product_uniques.product_id, product_uniques.expires_at, brand_id, productcode, products.name as product, brands.name as brand, categories.name as category, count(*) as amount'))
            ->groupBy('product_uniques.product_id', 'product_uniques.lot', 'product_uniques.expires_at', 'brand_id', 'products.productcode', 'products.name', 'brands.name', 'categories.name');
    }

    public function scopeGroupedUsed($query)
    {
        return $query->select(DB::raw('product_uniques.id, product_uniques.lot, product_uniques.product_id, product_uniques.used_at, brand_id, productcode, products.name as product, brands.name as brand, categories.name as category'))
            ->groupBy('product_uniques.id', 'product_uniques.product_id', 'product_uniques.lot', 'product_uniques.used_at', 'brand_id', 'products.productcode', 'products.name', 'brands.name', 'categories.name');
    }

    public function scopeExcludeUsed($query)
    {
        return $query->whereNull('product_uniques.used_at')
            ->whereNull('product_uniques.deleted_at')
            ->whereNull('product_uniques.returned_at');
    }

    public function scopeOnlyUsed($query)
    {
        return $query->whereNotNull('product_uniques.used_at');
    }

    /**
     * Generates all the needed sub parts, before adding a unique product
     * @param $organisation_id
     * @param array $product Needs: brand, category, location, name, productcode, price, lot, expires_at, amount
     * @throws Exception
     */
    public static function flexibleCreate($organisation_id, $product)
    {

        $required = ['brand', 'name', 'productcode'];
        foreach ($required as $fieldname) {
            if ( ! isset($product[$fieldname]) || empty($product[$fieldname])) {
                throw new Exception(ucfirst($fieldname) . ' is required');
            }
        }

        // Check or add brand cq supplier
        $brand = Brand::firstByAliasOrCreate(['name' => $product['brand']]);

        // Check or add category
        if (isset($product['category'])) {
            $category = Category::firstOrCreate(['organisation_id' => $organisation_id, 'name' => $product['category']]);
        } else {
            $category = Category::where('organisation_id', $organisation_id)->orderBy('id')->first();
        }

        // Check or add location
        if (isset($product['location'])) {
            $location = Location::firstOrCreate(['organisation_id' => $organisation_id, 'name' => $product['location']]);
        } else {
            $location = Location::where('organisation_id', $organisation_id)->orderBy('id')->first();
        }

        // Check or add product
        $baseProduct = [
            'organisation_id' => $organisation_id,
            'brand_id' => $brand->id,
            'category_id' => $category->id,
            'productcode' => $product['productcode']
        ];

        //test: $eloquentProduct = Product::firstByAttributes($baseProduct);
        $eloquentProduct = Product::where('organisation_id', $organisation_id)
                            ->where('brand_id', $brand->id)
                            ->where('category_id', $category->id)
                            ->where('productcode', $product['productcode'])
                            ->first();

        if (is_null($eloquentProduct)) {
            $baseProduct['price'] = round($product['price'], 2);
            $baseProduct['items_per_package'] = 1;
            $baseProduct['name'] = $product['name'];

            // why needed?
            //$baseProduct['user_id'] = 0;

            $eloquentProduct = Product::create($baseProduct);
        }

        // Add quantity of products to uniques
        $productUnique = new ProductUnique;
        $productUnique->product_id = $eloquentProduct->id;
        $productUnique->location_id = $location->id;
        $productUnique->lot = $product['lot'];
        //$productUnique->serial_number = $product['serial_number'];
        if (isset($product['expires_at']) && !empty($product['expires_at'])) {
            $productUnique->expires_at = $product['expires_at'];
        }
        $result = $productUnique->save();

        if ($result === false) {
            throw new Exception('Could not save product');
        }

        if (isset($product['amount']) && $product['amount'] > 1) {
            for ($i=0; $i < ($product['amount'] - 1); $i++) {
                $productUnique->replicate()->save();
            }
        }

        return $productUnique;
    }

    public static function prepareDate($expires_at_year, $expires_at_month, $expires_at_day = null)
    {
        if (!empty($expires_at_day)) {
            $day = $expires_at_day;
        } else {
            $day = 1;
        }
        $date = \Carbon\Carbon::createFromDate($expires_at_year, $expires_at_month, $day);

        if (empty($expires_at_day)) {
            return $date->lastOfMonth();
        } else {
            return $date;
        }
    }

    public function markUsed()
    {
        if (is_null($this->used_at)) {
            $this->used_at = $this->freshTimestamp();
            $this->update();
        }
    }

    public function reverseUsed()
    {
        $this->used_at = null;
        $this->update();
    }

    public function markReturned()
    {
        if (is_null($this->returned_at)) {
            $this->returned_at = $this->freshTimestamp();
            $this->update();
        }
    }

    public function reverseReturned()
    {
        $this->returned_at = null;
        $this->update();
    }

    public function scopeGetValue($query)
    {
        return $query->join('products', 'products.id', '=', 'product_uniques.product_id')->sum('price') / 100;
    }

    public function scopeGetCountAndValue($query)
    {
        /*echo $query->join('products', 'products.id', '=', 'product_uniques.product_id')
            ->select(DB::raw('count(*) as `count`, sum(price) / 100 as `value`'))
            ->toSql();*/

        return $query->join('products', 'products.id', '=', 'product_uniques.product_id')
            ->select(DB::raw('count(*) as `count`, sum(price) / 100 as `value`'))
            ->first();
    }
}
