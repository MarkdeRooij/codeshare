<?php namespace App\Http\Controllers;

use App\ProductUnique;
use App\Product;
use App\Brand;
use App\Location;
use Input;
use Validator;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Redirect;

class ProductUniquesController extends Controller
{

    protected $productUnique;
    protected $product;
    protected $location;
    protected $brand;

    public function __construct(ProductUnique $productUnique, Product $product, Location $location, Brand $brand)
    {
        $this->productUnique = $productUnique;
        $this->product = $product;
        $this->location = $location;
        $this->brand = $brand;
    }

    public function index()
    {

        // $productUnique->product->brand->name }}
        //{{ $productUnique->product->brand_id }}
        //{{ $productUnique->product->category->name

        // ->join('products', 'products.id', '=', 'product_uniques.product_id')

        $productUniques = $this->productUnique
            ->excludeUsed()
            ->join('products', function($join) {
                $join
                    ->on('products.id', '=', 'product_uniques.product_id');
            })
            ->join('brands', function($join) {
                $join
                    ->on('brands.id', '=', 'products.brand_id');
            })
            ->join('categories', function($join) {
                $join
                    ->on('categories.id', '=', 'products.category_id');
            })
            ->fromOrganisation(Auth::user()->department->organisation_id);

        if (Input::has('expires')) {
            switch (Input::get('expires')) {
                case 'expired':
                    $productUniques->expired();
                    break;
                case 'lessthen3months':
                    $productUniques->expiresBetween(date('Y-m-d'), \Config::get('mednext.expiration_3_months'));
                    break;
                case 'between3and6months':
                    $productUniques->expiresBetween(\Config::get('mednext.expiration_3_months'), \Config::get('mednext.expiration_6_months'));
                    break;
                case 'between6and12months':
                    $productUniques->expiresBetween(\Config::get('mednext.expiration_6_months'), \Config::get('mednext.expiration_12_months'));
                    break;
                case 'after12months':
                    $productUniques->expiresAfter(\Config::get('mednext.expiration_12_months'));
                    break;
                case 'never':
                    $productUniques->neverExpires();
                    break;
            }
        }

        // sort the table
        $order = 'asc';

        if (Input::has('order')) {
            $order = Input::get('order');
        }

        if (Input::has('sort') && in_array(Input::get('sort'), ['product', 'expires_at', 'productcode', 'price', 'amount'])) {
            $productUniques->orderBy(Input::get('sort'), $order);
        } else {
            $productUniques->orderBy('expires_at');
        }

        //$productUniques = $productUniques->toSql();
        //dd($productUniques);

        $productUniques = $productUniques
            ->grouped()
            ->paginate();

        // new
        $product = $this->product;
        $brand = $this->brand;

        $a = $productUniques;

        if ($order === 'asc') {
            $order = 'desc';
        } else {
            $order = 'asc';
        }

        return view('productuniques.index', compact('productUniques', 'product', 'brand', 'order'));
        //return view('productuniques.index', compact('brand', 'products'));
    }

    public function used()
    {
        // vervangen: ->fromOrganisation(Auth::user()->organisation_id)

        $productUniques = $this->productUnique
            ->onlyUsed()
            ->join('products', function($join) {
                $join
                    ->on('products.id', '=', 'product_uniques.product_id');
            })
            ->join('brands', function($join) {
                $join
                    ->on('brands.id', '=', 'products.brand_id');
            })
            ->join('categories', function($join) {
                $join
                    ->on('categories.id', '=', 'products.category_id');
            })
            ->fromOrganisation(Auth::user()->department->organisation_id);


        /*$productUniques = $this->productUnique
            ->fromOrganisation(Auth::user()->department->organisation_id)

            ->join('products', function($join) {
                $join
                    ->on('products.id', '=', 'product_uniques.product_id');
            })

            ->onlyUsed();*/

        if (Input::has('today')) {
            $productUniques->where('used_at', '>=', date('Y-m-d 00:00:00'));
        }

        // sort the table
        $order = 'asc';

        if (Input::has('order')) {
            $order = Input::get('order');
        }

        if (Input::has('sort') && in_array(Input::get('sort'), ['product', 'used_at', 'productcode'])) {
            $productUniques->orderBy(Input::get('sort'), $order);
        } else {
            $productUniques->orderBy('used_at', 'desc');
        }

        $productUniques = $productUniques
            ->groupedUsed()
            ->paginate();

        if ($order === 'asc') {
            $order = 'desc';
        } else {
            $order = 'asc';
        }

        return view('productuniques.used', compact('productUniques', 'order'));
    }

    public function added()
    {
        $productUniques = $this->productUnique
            ->fromOrganisation(Auth::user()->department->organisation_id)
            ->withTrashed()
            ->orderBy('created_at', 'desc');

        if (Input::has('today')) {
            $productUniques->where('created_at', '>=', date('Y-m-d'));
        }

        $productUniques = $productUniques->paginate();

        return view('productuniques.added', compact('productUniques'));
    }

    public function create($product_id)
    {
        $productUnique = $this->productUnique;
        $productUnique->amount = 1;

        $product = $this->product
            ->where('id', $product_id)
            ->where('organisation_id', Auth::user()->department->organisation_id)
            ->first();

        if (is_null($product)) {
            App::abort(404);
        }

        $productUnique->product_id = $product_id;

        if ( Input::has('location_id') ) {
            $location = $this->location
                ->where('id', Input::get('location_id'))
                ->where('organisation_id', Auth::user()->department->organisation_id)
                ->first();

            if (is_null($location)) {
                App::abort(404);
            }

            $productUnique->location_id = $location->id;
        } else {
            $location = $this->location;
        }

        return view('productuniques.create', compact('productUnique', 'product', 'location'));
    }

    public function store($product_id)
    {
        $input = Input::except('amount');
        $input['expires_at'] = ProductUnique::prepareDate($input['expires_at_year'], $input['expires_at_month'], $input['expires_at_day']);
        unset($input['expires_at_year'], $input['expires_at_month'], $input['expires_at_day']);
        $validation = Validator::make($input, ProductUnique::$rules);

        $product = $this->product
            ->where('id', $product_id)
            ->where('organisation_id', Auth::user()->department->organisation_id)
            ->first();

        if (is_null($product)) {
            App::abort(404);
        }

        $this->productUnique->product_id = $product->id;

        $location = $this->location
            ->where('id', Input::get('location_id'))
            ->where('organisation_id', Auth::user()->department->organisation_id)
            ->first();

        if (is_null($location)) {
            App::abort(404);
        }

        $this->productUnique->location_id = $location->id;

        if ($validation->passes()) {
            $this->productUnique->fill($input);
            $this->productUnique->save();

            if (Input::get('amount') > 1) {
                for ($i=0; $i < (Input::get('amount') - 1); $i++) {
                    $this->productUnique->replicate()->save();
                }
            }

            return Redirect::route('products.show', $product->id)
                ->with('message', 'messages.Product stock added');
        }

        return Redirect::route('products.uniques.create')
            ->withInput()
            ->withErrors($validation);
    }

    public function delete($id)
    {
        $productUnique = ProductUnique::where('product_id', $id)->firstOrFail();

        // use of product->organisation_id
        if ($productUnique->product->organisation_id != Auth::user()->department->organisation_id) {
            throw new Exception('Not your product');
        }

        $productUnique->delete();

        return Response::json([], 200);
    }

    public function destroy($id)
    {
        $productUnique = ProductUnique::where('id', $id)
            ->firstOrFail();

        if ($productUnique->product->organisation_id != Auth::user()->department->organisation_id) {
            throw new Exception('Not your product');
        }

        $productUnique->delete();

        return Response::json([], 200);
    }

}