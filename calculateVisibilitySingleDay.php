<?php

namespace App\Console\Commands;

use App\NewSerp;
use App\Visibility;
use Illuminate\Console\Command;
use DB;
use GearmanTask;
use PhpParser\Node\Expr\Print_;

class calculateVisibilitySingleDay extends Command {
	private $searchengine_id, $project_url, $project_name, $gearman_server;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'calcVisOneDay {date?}';
	
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Calculate Visibility For One Day Only';
	
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct ();
		$this->gearman_server = env('APP_GEARMAN','192.168.1.64');
		$this->gearman_server = '192.168.1.64';
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		
		$userDate = $this->argument('date');

		$projects = DB::table('projects')->where('project_active', 1)->orderBy('updated_at', 'desc')->orderBy('project_name', 'asc')->get();

		// to worker
		/*$client->setCompleteCallback ( function (GearmanTask $task, $context) use (&$returnPosArray) {
			switch ($context) {
				case 'calcProjectVisibility' :
					$returnPosArray ['lPos'] [] = json_decode ( $task->data (), true );
					break;
			}
		});*/

		$client = new \GearmanClient();
		$client->addServer( $this->gearman_server );

		foreach ($projects as $project) {

			$client->addTaskBackground('calcProjectVisibilityPerDay', json_encode(array(
					// get one model
					'projectId' => $project->id
					//'projectId' => 9
			)));

			//$client->runTasks();
			//return;

			print_r("projectid: ");
			print_r($project->id);
			print_r(PHP_EOL);
		}
		$client->runTasks();
		//$this->calcProjectVisibility($project->id);
	}

	public function getProjectName($projectId) {
		$projects = DB::table ( 'projects' )->where ( 'id', $projectId )->select ( 'project_name', 'project_url', 'updated_at', 'searchengine_id' )->first ();
		
		if (is_object ( $projects )) {
			$this->searchengine_id = ( int ) $projects->searchengine_id;
			$this->project_url = $projects->project_url;
			$this->project_name = $projects->project_name;
			return $this->project_name; // . $this->project_lastrun;
		}
		return false;
	}

	function getFirstPosition($keyword, $searchengineId) {
		$doc = NewSerp::where ( 'keyword', $keyword )->where ( 'searchengine_id', ( int ) $searchengineId )->orderBy ( 'date', 'asc' )->first ();
		
		if (is_object ( $doc )) {
			$serpData = json_decode ( $doc->top100 );
			
			if (is_array ( $serpData ) || property_exists ( $serpData, 'error' )) {
				return false;
			}
			foreach ( $serpData as $key => $value ) {
				if (strpos ( $value, $this->project_url ) !== false) {
					return $key;
				}
			}
			return false;
		}
	}
}
