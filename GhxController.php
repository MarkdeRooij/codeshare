<?php namespace App\Http\Controllers;

use Auth;
use DB;
use App\Product;
use App\ProductUniqueUsage;
use App\ProductUnique;
use App\IncomingMessage;
use App\IncomingMessageId;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;

class GhxController extends Controller
{

    protected $product, $request;

    public function __construct(Product $product, Request $request)
    {
        $this->product = $product;
        $this->request = $request;
    }

    public function incomingProduct()
    {
        $incomingMessage = IncomingMessage::find(\Config::get('incoming_message_id'));

        $request = $this->request->getContent();
        if (empty($request)) {
            //return Response::make('No content found', 400);
            return response('No content found', 400)->header('Content-Type', 'text/plain');
        }

        $xml = simplexml_load_string($request);
        if ($xml === false) {
            //return Response::make('Not valid XML', 400);
            return response('Not valid XML', 400)->header('Content-Type', 'text/plain');
        }

        if ( ! isset($xml->scandata->scanline) || count($xml->scandata->scanline) == 0 ) {
            //return Response::make('No scandata found', 400);
            return response('No scandata found', 400)->header('Content-Type', 'text/plain');
        }

        DB::beginTransaction();

        try {
            $results = array();
            $processedIds = array();

            foreach ($xml->scandata->scanline as $scanline) {
                // Only process line, if we never saw this lineid before
                if (IncomingMessageId::where('lineid', $scanline->scanlineid)->count() == 0)
                {
                    $product = [
                        'productcode' => (string) $scanline->productnumber,
                        'name' => (string) $scanline->productname,
                        'brand' => (string) $scanline->supplier,
                        'lot' => (string) $scanline->lotnumber,
                        'price' => (string) $scanline->price,
                        'expires_at' => (string) $scanline->preservedate,
                        'location' => (string) $scanline->location1,
                        'amount' => (string) $scanline->quantity
                    ];

                    if (empty($product['name']))
                    {
                        $product['name'] = 'Unknown';
                    }

                    $results[] = ProductUnique::flexibleCreate(Auth::user()->department->organisation_id, $product);
                    $processedIds[] = $scanline->scanlineid;
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            $error = 'Could not process data: ' . $e->getMessage() . ' on line ' . $e->getLine() . ' from ' . $e->getFile();

            $incomingMessage->failed($error);

            //return Response::make($error, 400);
            return response($error, 400)->header('Content-Type', 'text/plain');
        }

        $result = array();
        foreach ($results as $resultrule)
        {
            $result[] = array('product_unique_id' => $resultrule->id);
        }

        $incomingMessage->setResult($result);
        $incomingMessage->succeeded();

        $incomingMessage->registerLineIds($processedIds);

        DB::commit();

        $response = '<?xml version="1.0" encoding="UTF-8"?><result><success>true</success><errors>false</errors><message_id>' . $incomingMessage->id . '</message_id></result>';

        //return Response::make($response, 200);
        return response($response, 200)->header('Content-Type', 'text/plain');

    }

    public function relocate()
    {
        $incomingMessage = IncomingMessage::find(config('incoming_message_id'));

        $request = $this->request->getContent();
        if (empty($request)) {
            //return Response::make('No content found', 400);
            return response('No content found', 400)->header('Content-Type', 'text/plain');
        }

        $xml = simplexml_load_string($request);
        if ($xml === false) {
            //return Response::make('Not valid XML', 400);
            return response('Not valid XML', 400)->header('Content-Type', 'text/plain');
        }

        if ( ! isset($xml->scandata->scanline) || count($xml->scandata->scanline) == 0 ) {
            //return Response::make('No scandata found', 400);
            return response('No scandata found', 400)->header('Content-Type', 'text/plain');
        }

        // Start transaction
        DB::beginTransaction();

        try {
            $results = array();
            $processedIds = array();

            foreach ($xml->scandata->scanline as $scanline) {
                // Only process line, if we never saw this lineid before
                if (IncomingMessageId::where('lineid', $scanline->scanlineid)->count() == 0)
                {
                    $product = [
                        'productcode' => (string) $scanline->productnumber,
                        'name' => (string) $scanline->productname,
                        'brand' => (string) $scanline->supplier,
                        //'serial_number' => (string) $scanline->serialnumber,
                        'lot' => (string) $scanline->lotnumber,
                        'fromlocation1' => (string) $scanline->fromlocation1,
                        'fromlocation2' => (string) $scanline->fromlocation2,
                        'fromlocation3' => (string) $scanline->fromlocation3,
                        'tolocation1' => (string) $scanline->tolocation1,
                        'tolocation2' => (string) $scanline->tolocation2,
                        'tolocation3' => (string) $scanline->tolocation3,
                        'price' => (string) $scanline->price,
                        'expires_at' => (string) $scanline->preservedate,
                        'location' => (string) $scanline->location1,
                        'amount' => (string) $scanline->quantity
                    ];

                    if (empty($product['name']))
                    {
                        $product['name'] = 'Unknown';
                    }

                    dd($product);

                    // no flexible create needed
                    //$results[] = ProductUnique::flexibleCreate(Auth::user()->department->organisation_id, $product);
                    $processedIds[] = $scanline->scanlineid;
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            $error = 'Could not process data: ' . $e->getMessage() . ' on line ' . $e->getLine() . ' from ' . $e->getFile();

            $incomingMessage->failed($error);

            //return Response::make($error, 400);
            return response($error, 400)->header('Content-Type', 'text/plain');
        }

        /*
        $result = array();
        foreach ($results as $resultrule)
        {
            $result[] = array('product_unique_id' => $resultrule->id);
        }

        $incomingMessage->setResult($result);
        $incomingMessage->succeeded();

        $incomingMessage->registerLineIds($processedIds);*/

        DB::commit();

        $response = '<?xml version="1.0" encoding="UTF-8"?><result><success>true</success><errors>false</errors><message_id>' . $incomingMessage->id . '</message_id></result>';

        //return Response::make($response, 200);
        return response($response, 200)->header('Content-Type', 'text/plain');

    }

    public function registerUsage()
    {
        $incomingMessage = IncomingMessage::find(\Config::get('incoming_message_id'));

        $request = $this->request->getContent();
        if (empty($request)) {
            //return Response::make('No content found', 400);
            return response('No content found', 400)->header('Content-Type', 'text/plain');
        }

        $xml = simplexml_load_string($request);
        if ($xml === false) {
            //return Response::make('Not valid XML', 400);
            return response('Not valid XML', 400)->header('Content-Type', 'text/plain');
        }

        if ( ! isset($xml->scandata->scanline) || count($xml->scandata->scanline) == 0 ) {
            //return Response::make('No scandata found', 400);
            return response('No scandata found', 400)->header('Content-Type', 'text/plain');
        }

        DB::beginTransaction();

        try {
            $results = array();

            foreach ($xml->scandata->scanline as $scanline) {
                $product = [
                    'productcode' => (string) $scanline->productnumber,
                    'name' => (string) $scanline->productname,
                    'brand' => (string) $scanline->supplier,
                    'lot' => (string) $scanline->lotnumber,
                    'price' => (string) $scanline->price,
                    'expires_at' => (string) $scanline->preservedate,
                    'location' => (string) $scanline->location1,
                    'usage_date' => date('Y-m-d', strtotime( (string) $scanline->scantime)),
                    'patient' => (string) $scanline->patient,
                    'specialist' => (string) $scanline->medic,
                    'reference' => (string) $scanline->sessionnumber,
                ];

                for ($i = 0; $i < (int)$scanline->quantity; $i++) {
                    $results[] = ProductUniqueUsage::flexibleCreate(Auth::user()->department->organisation_id, $product);
                }
            }
        } catch (Exception $e) {
            DB::rollback();

            $error = 'Could not process data: ' . $e->getMessage() . ' on line ' . $e->getLine() . ' from ' . $e->getFile();

            $incomingMessage->failed($error);

            //return Response::make($error, 400);
            return response($error, 400)->header('Content-Type', 'text/plain');
        }

        $result = array();
        foreach ($results as $resultrule) {
            if ($resultrule instanceof ProductUnique) {
                $result[] = array('product_unique_id' => $resultrule->id);
            } elseif ($resultrule instanceof UsageCustomRule) {
                $result[] = array('usage_custom_rule_id' => $resultrule->id, 'usage_id' => $resultrule->usage_id);
            }
        }

        $incomingMessage->setResult($result);
        $incomingMessage->succeeded();

        DB::commit();

        $response = '<?xml version="1.0" encoding="UTF-8"?><result><success>true</success><errors>false</errors><message_id>' . $incomingMessage->id . '</message_id></result>';

        //return Response::make($response, 200);
        return response($response, 200)->header('Content-Type', 'text/plain');
    }

}