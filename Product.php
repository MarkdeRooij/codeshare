<?php namespace App;

use Mednext\Exceptions\ModelDeleteException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $guarded = array();

    public static $rules = array(
        'name' => 'required'
    );

    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function productStockLevels()
    {
        return $this->hasMany('App\ProductStockLevel');
    }

    public function productUniques()
    {
        return $this->hasMany('App\ProductUnique');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getPriceAttribute($value)
    {
        if (is_null($value)) {
            return null;
        }
        return $value / 100;
    }

    public function setPriceAttribute($value)
    {
        if ($value === '') {
            $this->attributes['price'] = null;
        } else {
            $this->attributes['price'] = round(str_replace(',', '.', $value) * 100);
        }
    }

    public function setBrandIdAttribute($value)
    {
        if ($value == 0) {
            $value = null;
        }
        $this->attributes['brand_id'] = $value;
    }

    public function getStock($location = null)
    {
        $uniques = $this->productUniques()->excludeUsed();
        if (!is_null($location)) {
            $uniques = $uniques->where('location_id', $location);
        }
        return $uniques->count();
    }

    public function getProductUniquesGrouped()
    {
        $uniqueProducts = $this->productUniques()
            ->excludeUsed()
            ->select(\DB::raw('lot, expires_at, created_at, location_id, count(*) as amount'))
            ->groupBy('lot', 'expires_at', 'created_at', 'location_id');
            //->get();

        return $uniqueProducts;
    }

    public function scopeSearch($query, $search)
    {
        return $query->where(
            function($q) use ($search) {
                $q->where('products.productcode', 'LIKE', '%' . $search . '%')
                    ->orWhere('products.name', 'LIKE', '%' . $search . '%');
            }
        );
    }

    /**
     * Check if model is deletable
     * @return bool true if model is deletable
     * @throws Mednext\Exceptions\ModelDeleteException
     */
    public function checkDelete()
    {
        if ($this->productUniques->count() > 0)
            throw new ModelDeleteException('Product contains unique items');

        return true;
    }

    public function deletable()
    {
        try {
            $this->checkDelete();
        } catch (ModelDeleteException $e) {
            return false;
        }

        return true;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function($model) {
            $model->checkDelete();
        });
    }
}
