<?php

namespace App\Http\Middleware;

use Closure;
//use Illuminate\Support\Facades\Auth;
// use Illuminate\Http\Request;
use App\IncomingMessage;

class BeforeApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $incomingMessage = new IncomingMessage();
        $incomingMessage->organisation_id = \Auth::user()->department->organisation_id;
        $incomingMessage->type = $request->getRequestUri();
        $incomingMessage->origin_ip = $_SERVER['REMOTE_ADDR'];
        $incomingMessage->message = $request->getContent();
        $incomingMessage->save();

        // \Config::set('incoming_message_id', $incomingMessage->id);
        config(['incoming_message_id' => $incomingMessage->id]);

        return $next($request);
    }
}
