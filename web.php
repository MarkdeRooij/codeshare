<?php

Route::get('login', ['uses' => 'LoginController@form', 'as' => 'login']);
Route::post('login', ['uses' => 'LoginController@login', 'as' => 'login.login']);

//Gate::resource('posts', 'PostPolicy');



Route::group(['middleware' => ['auth', 'set-language']], function() {
//Route::group(['before' => 'auth'], function() {

	Route::group(['middleware' => ['role:admin']], function () {
        //Route::group(['middleware' => ['auth','admin']], function() {
        // new Users
        //Route::get('users', ['uses' => 'UsersController@index', 'as' => 'users.index']);
        Route::resource('users', 'UsersController');

        // new OrganisationsController
        Route::get('organisations', ['uses' => 'OrganisationsController@index', 'as' => 'organisations']);
        Route::get('organisations', ['uses' => 'OrganisationsController@index', 'as' => 'organisations.index']);
        // Get Data
        Route::get('organisations/getdata', 'OrganisationsController@getPosts')->name('organisations/getdata');;

        Route::get('organisations', ['uses' => 'OrganisationsController@index', 'as' => 'organisations']);
        Route::get('organisations', ['uses' => 'OrganisationsController@index', 'as' => 'organisations.index']);
        Route::get('organisations/show/{id}', ['uses' => 'OrganisationsController@show', 'as' => 'organisations.show']);
        Route::get('organisations/create', ['uses' => 'OrganisationsController@create', 'as' => 'organisations.create']);
        Route::post('organisations/{id?}', ['uses' => 'OrganisationsController@store', 'as' => 'organisations.store']);
        //Route::get('products/used', ['uses' => 'ProductUniquesController@used', 'as' => 'products.uniques.used']);

        // new DepartmentsController
        Route::resource('departments', 'DepartmentsController');
	});


    Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);
    Route::get('stats', ['uses' => 'StatsController@index', 'as' => 'stats']);
    Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);

    Route::resource('locations', 'LocationsController');

    // Display view
    Route::get('datatable', 'DataTableController@datatable');
    // Get Data
    Route::get('datatable/getdata', 'DataTableController@getPosts')->name('datatable/getdata');

    /*
    Route::get('datatables/anyData', [
        'as' => 'datatables.anyData',
        'uses' => 'DatatablesController@anyData'
    ]);

    Route::get('datatables', [
        'as' => 'datatables.index',
        'uses' => 'DatatablesController@getIndex'
    ]);
    */

    Route::get('products/used', ['uses' => 'ProductUniquesController@used', 'as' => 'products.uniques.used']);
    Route::get('products/added', ['uses' => 'ProductUniquesController@added', 'as' => 'products.uniques.added']);

    //Route::get('products/getdata', 'ProductsController@getPosts')->name('products/getdata');
    Route::resource('products', 'ProductsController');

    Route::get('products/{id}/recall', ['uses' => 'ProductRecallsController@show', 'as' => 'products.recalls.show']);
    Route::get('products/{id}/stocklevels/{location_id}/edit', ['uses' => 'ProductStockLevelsController@edit', 'as' => 'products.stocklevel.edit']);
    Route::patch('products/{id}/stocklevels/{location_id}', ['uses' => 'ProductStockLevelsController@update', 'as' => 'products.stocklevel.update']);

    Route::get('products/{id}/uniques/create', ['uses' => 'ProductUniquesController@create', 'as' => 'products.uniques.create']);
    Route::post('products/{id}/uniques', ['uses' => 'ProductUniquesController@store', 'as' => 'products.uniques.store']);

    Route::get('uniques', ['uses' => 'ProductUniquesController@index', 'as' => 'products.uniques.index']);

    //Route::delete('uniques/{id}', ['uses' => 'ProductUniquesController@destroy', 'as' => 'products.uniques.destroy']);
    Route::delete('uniques/delete/{id}',['as' => 'products.uniques.delete', 'uses' => 'ProductUniquesController@delete']);


    //Route::delete('uniques','ProductUniquesController@delete');


    Route::get('quick-usage', ['uses' => 'QuickUsageController@index', 'as' => 'quickusage.index']);
    Route::get('quick-usage/search', ['uses' => 'QuickUsageController@search', 'as' => 'quickusage.search']);
    Route::post('quick-usage', ['uses' => 'QuickUsageController@register', 'as' => 'quickusage.register']);

    Route::resource('usages', 'UsagesController');
    Route::post('usages/{id}/products', ['uses' => 'UsagesController@addProduct', 'as' => 'usages.products.store']);
    Route::get('usages/{id}/products', ['uses' => 'UsagesController@getProducts', 'as' => 'usages.products.index']);
    Route::get('usages/{id}/close', ['uses' => 'UsagesController@close', 'as' => 'usages.close']);
    Route::patch('usages/{id}/products/{product_unique_usage_id}', ['uses' => 'UsagesController@changeProductType', 'as' => 'usages.products.change-product-type']);
    Route::delete('usages/{id}/products/{product_unique_usage_id}', ['uses' => 'UsagesController@deleteProduct', 'as' => 'usages.products.delete']);
    Route::delete('usages/{id}/customrules/{ruleid}', ['uses' => 'UsagesController@deleteCustomRule', 'as' => 'usages.customrules.delete']);

    Route::resource('return-shipments', 'ReturnShipmentsController');
    Route::post('return-shipments/{id}/products', ['uses' => 'ReturnShipmentsController@addProduct', 'as' => 'return-shipments.products.store']);
    Route::get('return-shipments/{id}/products', ['uses' => 'ReturnShipmentsController@getProducts', 'as' => 'return-shipments.products.index']);
    Route::get('return-shipments/{id}/markreturned', ['uses' => 'ReturnShipmentsController@markReturned', 'as' => 'return-shipments.markreturned']);
    Route::delete('return-shipments/{id}/products/{product_unique_usage_id}', ['uses' => 'ReturnShipmentsController@deleteProduct', 'as' => 'return-shipments.products.delete']);
    Route::get('return-shipments/{id}/pdf', ['uses' => 'ReturnShipmentsController@pdf', 'as' => 'return-shipments.pdf']);

    Route::get('import', ['uses' => 'ImportController@create', 'as' => 'import.create']);
    Route::post('import', ['uses' => 'ImportController@store', 'as' => 'import.store']);

    Route::get('incomingmessages', ['uses' => 'IncomingMessagesController@index', 'as' => 'incomingmessages.index']);
    Route::get('incomingmessages/{id}', ['uses' => 'IncomingMessagesController@show', 'as' => 'incomingmessages.show']);

    Route::post('questions', ['uses' => 'QuestionsController@store', 'as' => 'questions.store']);

    Route::resource('brands', 'BrandsController');

    Route::resource('exports', 'ExportsController');
});

//Route::middleware(['first', 'second'])->group(function () {
//Route::group(['middleware' => 'auth'], function() {

/*
Route::group(['prefix' => 'api/v1', 'middleware' => ['api-auth', 'before-api'], function() {
    // All routes related to an API
    Route::post('ghx/incoming', ['uses' => 'GhxController@incomingProduct']);
    Route::post('ghx/relocate', ['uses' => 'GhxController@relocate']);
    Route::post('ghx/usage', ['uses' => 'GhxController@registerUsage']);
}]);
*/

//Route::group(array('prefix' => 'api/v1', 'middleware' => ['api-auth', 'before-api']), function () {
Route::group(array('prefix' => 'api/v1', 'middleware' => ['api-auth', 'before-api']), function () {
    Route::post('ghx/incoming', ['uses' => 'GhxController@incomingProduct']);
    Route::post('ghx/relocate', ['uses' => 'GhxController@relocate']);
    Route::post('ghx/usage', ['uses' => 'GhxController@registerUsage']);
});

/*Route::get('/', function () {
    return view('welcome');
});*/

//Auth::routes();

Route::get('/home', 'HomeController@index');
