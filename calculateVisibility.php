<?php

namespace App\Console\Commands;

use App\NewSerp;
use App\Visibility;
use Illuminate\Console\Command;
use DB;
use GearmanTask;
use PhpParser\Node\Expr\Print_;

class calculateVisibility extends Command {
	private $searchengine_id, $project_url, $project_name, $gearman_server;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'calcVis {projectId}';
	
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Calculate Visibility';
	
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct ();
		$this->gearman_server = env('APP_GEARMAN','192.168.1.64');
		$this->gearman_server = '192.168.1.64';
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$projectId = $this->argument ( 'projectId' );

		if($projectId != 'all') {
			$this->calcProjectVisibility($projectId);
			return;
		}
		$projects = DB::table('projects')->orderBy('updated_at', 'desc')->orderBy('project_name', 'asc')->get();

		// to worker
		/*$client->setCompleteCallback ( function (GearmanTask $task, $context) use (&$returnPosArray) {
			switch ($context) {
				case 'calcProjectVisibility' :
					$returnPosArray ['lPos'] [] = json_decode ( $task->data (), true );
					break;
			}
		});*/

		$client = new \GearmanClient();
		$client->addServer( $this->gearman_server );

		foreach ($projects as $project) {
			//echo $project->id;
			$client->addTaskBackground('calcProjectVisibility', json_encode(array(
					// get one model
					//'projectId' => 9
					'projectId' => $project->id
			)));
			print_r("projectid: ");
			print_r($project->id);
			print_r(PHP_EOL);
			
		}

		$client->runTasks();
		//$this->calcProjectVisibility($project->id);
	}


	public function calcProjectVisibility($projectId) {
		//$this->getProjectName( $projectId );
		$client = new \GearmanClient();
		$client->addServer( $this->gearman_server );

		$keyword = DB::table ( 'keywords' )->where ( 'project_id', $projectId )->first();
		if(!isset($keyword->keyword )) {
			return false;
		}
		$docsLast = NewSerp::where ( 'keyword', $keyword->keyword )->where ( 'searchengine_id', ( int ) $this->searchengine_id )->orderBy ( 'date', 'desc' )->first();

		if(!is_object($docsLast)) {
			echo 'no object';
			return;
		}

		// get first and last date 
		$lastDate = substr ( $docsLast->date, 0, 10 );
		$format = 'Y-m-d';
		$lastRunDate = \DateTime::createFromFormat ( $format, $lastDate );

		$docsFirst = NewSerp::where ( 'keyword', $keyword->keyword )->where ( 'searchengine_id', ( int ) $this->searchengine_id )->orderBy ( 'date', 'asc' )->first ();
		$firstDate = substr ( $docsFirst->date, 0, 10 );
		$format = 'Y-m-d';
		$firstRunDate = \DateTime::createFromFormat ( $format, $firstDate );

		// $firstRunDate = new DateTime( '2010-05-01' );
		// $lastRunDate = new DateTime( '2010-05-10' );

		$interval = \DateInterval::createFromDateString ( '1 day' );
		$period = new \DatePeriod ( $firstRunDate, $interval, $lastRunDate );

		foreach ( $period as $dt ) {

			//$request = \Request::create('/APIVisibility/'.$projectId.'?date='. urlencode($dt->format( "Y-m-d" )), 'GET');
			//$instance = json_decode(Route::dispatch($request)->getContent());
			
			$crlUrl = "http://lavaserp.test/APIVisibility/" . $projectId . '?date='. urlencode($dt->format( "Y-m-d" ));
			$apiKey = "X-SRM-Api-Key:9c60479061b1031bd75aaeb13bc6ff58";

			$ch = curl_init();
			$request_headers [] = $apiKey;
			
			$curl_options = array (
					CURLOPT_HTTPHEADER => $request_headers,
					//CURLOPT_ENCODING => 'gzip',
					CURLOPT_CONNECTTIMEOUT => 1,
					//CURLOPT_SSL_VERIFYPEER => false
			);
			
			// quick fix ssl
			//curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
			// set url
			curl_setopt ( $ch, CURLOPT_URL, $crlUrl );
			// set the auto header
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $request_headers );
			// return the transfer as a string
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );

			// $output contains the output string
			$visData = curl_exec( $ch );
			$errType = '';
			if (!curl_errno($ch)) {

				$result = json_decode($visData);

				//if (json_last_error() === JSON_ERROR_NONE) {
			}

			//curl_setopt($ch, CURLOPT_URL, 'http://lavaserp.test/APIVisibility/12?projectId=' . urlencode($projectId) . '&' . urlencode($dt->format( "Y-m-d" )));
			//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//$data = curl_exec($ch);

			// Here we queue up multiple tasks to be execute in *as much* parallelism as gearmand can give us
			$client->addTask ( 'calcAndInsertVisibility', json_encode ( array (
					// get one model
					'project_id' => $projectId,
					'date' => $dt->format ( "Y-m-d" ),
					'visibility_self' => $visPercentages ['svSelf'],
					'visibility_other' => $visPercentages ['svOther']
			) ), 'calcAndInsertVisibility' );

			//echo $projectId . ' - ' . $dt->format ( "Y-m-d" ) . PHP_EOL;
			// to be placed in worker
			$visPercentages = app ('App\Http\Controllers\VisibilityController' )->calcVisibilityPercentagesByDate ( $projectId, $dt->format ( "d-m-Y" ) );
			//print_r ( $visPercentages );
			
			// to be placed in worker
			Visibility::create ( [
					'project_id' => $projectId,
					'date' => $dt->format ( "Y-m-d" ),
					'visibility_self' => $visPercentages ['svSelf'],
					'visibility_other' => $visPercentages ['svOther']
			] );
		}
	}


	public function getProjectName($projectId) {
		$projects = DB::table ( 'projects' )->where ( 'id', $projectId )->select ( 'project_name', 'project_url', 'updated_at', 'searchengine_id' )->first ();
		
		if (is_object ( $projects )) {
			$this->searchengine_id = ( int ) $projects->searchengine_id;
			$this->project_url = $projects->project_url;
			$this->project_name = $projects->project_name;
			return $this->project_name; // . $this->project_lastrun;
		}
		return false;
	}

	function getFirstPosition($keyword, $searchengineId) {
		$doc = NewSerp::where ( 'keyword', $keyword )->where ( 'searchengine_id', ( int ) $searchengineId )->orderBy ( 'date', 'asc' )->first ();
		
		if (is_object ( $doc )) {
			$serpData = json_decode ( $doc->top100 );
			
			if (is_array ( $serpData ) || property_exists ( $serpData, 'error' )) {
				return false;
			}
			foreach ( $serpData as $key => $value ) {
				if (strpos ( $value, $this->project_url ) !== false) {
					return $key;
				}
			}
			return false;
		}
	}
}
