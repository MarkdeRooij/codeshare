@extends('master')
@section('main')

<div class="page-header">
    <div class="button-bar pull-right">
        <a href="{!! route('products.index') !!}" class="btn btn-default"><i class="fa fa-arrow-left"></i> @lang('messages.Back to products')</a>
    </div>
    <h1>
        @lang('messages.Unique products')
        <small><?php
        if (Input::has('expires')) {
            switch (Input::get('expires')) {
                case 'expired':
                    echo Lang::get('messages.Already expired');
                    break;
                case 'lessthen3months':
                    echo Lang::get('messages.Expires in 3 months');
                    break;
                case 'between3and6months':
                    echo Lang::get('messages.Expires between 3 and 6 months');
                    break;
                case 'between6and12months':
                    echo Lang::get('messages.Expires between 6 and 12 months');
                    break;
                case 'after12months':
                    echo Lang::get('messages.Expires after 12 months');
                    break;
                case 'never':
                    echo Lang::get('messages.Never expires');
                    break;
            }
        }
        ?></small>
    </h1>
</div>

@include('partials.messages')

@if (count($productUniques) > 0)
    {!! $productUniques->appends(Request::except('page'))->render() !!}
    <table class="table table-striped">
        <thead>
            <tr>
                <th><a href="?{!! http_build_query(Request::except(['page', 'sort', 'order']) + ['sort' => 'productcode'] + ['order' => $order]) !!}">@lang('messages.Product code')</a></th>
                <th><a href="?{!! http_build_query(Request::except(['page', 'sort', 'order']) + ['sort' => 'product'] + ['order' => $order]) !!}">@lang('messages.Name')</a></th>
                <th>@lang('messages.Brand')</th>
                <th>@lang('messages.Category')</th>
                <th>@lang('messages.Lot')</th>
                <th><a href="?{{ http_build_query(Request::except(['page', 'sort', 'order']) + ['sort' => 'expires_at'] + ['order' => $order])}}">@lang('messages.Expires at')</a></th>
                <th class="align-right"><a href="?{!! http_build_query(Request::except(['page', 'sort', 'order']) + ['sort' => 'amount'] + ['order' => $order]) !!}">@lang('messages.Amount')</a></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($productUniques as $productUnique)
                <tr>
                    <td>{{ $productUnique->productcode}}</td>
                    <td>{!! link_to_route('products.show', $productUnique->product, [$productUnique->product_id]) !!}</td>
                    <td>
                        {{-- @if (!empty($productUnique->product->brand_id)) --}}
                            {{ $productUnique->brand }}
                            {{ $productUnique->brand_id }}
                        {{--@endif--}}
                    </td>
                    <td>{{ $productUnique->category }}</td>
                    <td>{{ $productUnique->lot}}</td>
                    <td>@expirationDate($productUnique->expires_at)</td>
                    <td class="align-right">{{ $productUnique->amount }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {!! $productUniques->appends(Request::except('page'))->render() !!}
@else
    <p>{{ trans('messages.No products added')}}.</p>
@endif

@stop